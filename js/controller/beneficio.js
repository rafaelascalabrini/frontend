var beneficiosModulo = angular.module('beneficiosModulo',[])

beneficiosModulo.controller("beneficiosController", function ($scope){

    $scope.beneficios = [
        {id: 1, nome: 'Renda Mínima', esfera: 'Municipal'}
    ];

    $scope.selecionaBeneficio = function(beneficioSelecionado){
        $scope.beneficio = beneficioSelecionado;
    }

    $scope.novoBeneficio = function(){
        $scope.beneficio = "";
    }

    $scope.zerarFormulario = function() {
        $scope.formulario.$setPristine();
        $scope.formulario.$setUntouched();
    }

    $scope.salvarBeneficio = function(){
        $scope.formulario.$setDirty();

        if($scope.formulario.$invalid)
            return;

        $scope.beneficios.push($scope.beneficio);
        $scope.novoBeneficio();
    }

    $scope.excluirBeneficio = function(){
        $scope.beneficios.splice($scope.beneficios.indexOf($scope.beneficio),1);
    }

});
